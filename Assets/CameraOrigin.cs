﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrigin : MonoBehaviour {

	float prev_x = 0f;
	float prev_y = 0f;
	Vector2 rotation;
	MainObject main;

	// Use this for initialization
	void Start () {
		rotation = new Vector2();
		main = GameObject.Find("MainObject").GetComponent<MainObject>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			OnMouseDown();
		} else if (Input.GetMouseButton(0)) {
			OnMouseDrag();
		}
	}

	void OnMouseDown() {
		Vector3 pos = Input.mousePosition;
		prev_x = pos.x;
		prev_y = pos.y;
  }

	void OnMouseDrag() {
		if (!main.isRotatingCamera) {
			return;
		}
		Vector3 pos = Input.mousePosition;
		float delta_x = pos.x - prev_x;
		float delta_y = pos.y - prev_y;

		float rate = 0.2f;
		rotation.x -= delta_y * rate;
		rotation.y += delta_x * rate;

		if (rotation.x > 90) {
			rotation.x = 90;
		}

		if (rotation.x < -90) {
			rotation.x = -90;
		}

		transform.rotation = UnityEngine.Quaternion.identity;
		transform.Rotate(rotation.x, rotation.y, 0);

		prev_x = pos.x;
		prev_y = pos.y;
  }
}
