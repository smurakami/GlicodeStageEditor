﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using System.Text.RegularExpressions;
using SFB;

public class MainObject : MonoBehaviour {

  public GameObject chipPrefab;
  public GameObject sampleChipObject;
  MapChip sampleChip;
  public bool isRotatingCamera;

  MapChip[,] map;
  public int mapSize = 10;

	// Use this for initialization
	void Start () {
    isRotatingCamera = false;
    const int mapSize = 10;
    map = new MapChip[mapSize, mapSize];
    for (int i = 0; i < mapSize; i++) {
      for (int j = 0; j < mapSize; j++) {
        float x = j - mapSize * 0.5f;
        float z = i - mapSize * 0.5f;

        var obj = Instantiate(chipPrefab, new Vector3(x, 0f, z), Quaternion.identity) as GameObject;
        MapChip chip = obj.GetComponent<MapChip>();
        chip.index.i = i;
        chip.index.j = j;
        map[i, j] = chip;
      }
    }

    sampleChip = sampleChipObject.GetComponent<MapChip>();
    sampleChip.UpdateState();
  }

	// Update is called once per fram
	void Update () {
    if (Input.GetKeyDown(KeyCode.DownArrow)) {
      sampleChip.alt -= 1;
      if (sampleChip.alt < 0) {
        sampleChip.alt = 0;
      }
      sampleChip.UpdateState();
    }
    if (Input.GetKeyDown(KeyCode.UpArrow)) {
      sampleChip.alt += 1;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.N) || Input.GetKeyDown(KeyCode.F)) { // normal, field
      sampleChip.type = MapChip.Type.Normal;
      sampleChip.top = MapChip.Top.None;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.E)) { // Enter
      sampleChip.type = MapChip.Type.Normal;
      sampleChip.top = MapChip.Top.Enter;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.J)) { // Jump
      sampleChip.type = MapChip.Type.Jump;
      sampleChip.top = MapChip.Top.None;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.T)) { // Tree
      sampleChip.type = MapChip.Type.Normal;
      sampleChip.top = MapChip.Top.Tree;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.C)) { // Clear
      sampleChip.type = MapChip.Type.None;
      sampleChip.top = MapChip.Top.None;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.P)) { // Pipe
      sampleChip.type = MapChip.Type.Normal;
      sampleChip.top = MapChip.Top.Pipe;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.S)) { // Stone
      sampleChip.type = MapChip.Type.Normal;
      sampleChip.top = MapChip.Top.Stone;
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.R)) { // Rotate
      sampleChip.direction += 1;
      if (sampleChip.direction >= 4) {
        sampleChip.direction = 0;
      }
      sampleChip.UpdateState();
    }

    if (Input.GetKeyDown(KeyCode.W)) { // Water
      sampleChip.type = MapChip.Type.River;
      sampleChip.top = MapChip.Top.None;
      sampleChip.UpdateState();
    }

    if (Input.GetMouseButtonDown(0)) {
      RaycastHit hit;
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      if (Physics.Raycast(ray, out hit)) {
        isRotatingCamera = false;
      } else {
        isRotatingCamera = true;
      }
    }
	}

  public void Save() {
    StartCoroutine(SavePNGAndJSON());
    // fromJSON(json);
  }

  public void Load() {
    // string path = StandaloneFileBrowser.OpenFilePanel(
    string path = StandaloneFileBrowser.OpenFilePanel(
      "JSONファイルを選択してください", 
      System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory),
      "json", false)[0].Replace("file://", "");

    FileInfo fi = new FileInfo(path);
    using (StreamReader sr = new StreamReader(fi.OpenRead())){
        string json = sr.ReadToEnd();
        fromJSON(json);
    }
  }

  public string toJSON() {
    string [] all = new string[mapSize];
    for (int i = mapSize - 1; i >= 0; i--) {
      // all += "  [";
      string[] row = new String[mapSize];
      for (int j = 0; j < mapSize; j++) {
        row[j] = map[i, j].toJSON();
      }
      all[mapSize - i - 1] = "  [" + string.Join(", ", row) + "]";
    }
    string allString = "[\n" + string.Join(",\n", all) + "]";
    return allString;
  }

  public void fromJSON(string allString) {
    string[] delimiter = {",\n"};
    string[] all = allString.Substring(2, allString.Length - 3).Split(delimiter, StringSplitOptions.None);
    for (int i = 0; i < mapSize; i++) {
      string[] delimiter_ = {", "};
      string[] row = all[i].Substring(3, all[i].Length - 4).Split(delimiter_, StringSplitOptions.None);
      for (int j = 0; j < mapSize; j++) {
        map[mapSize - i - 1, j].readJSON(row[j]);
      }
    }
  }

   // 画像の保存先パス取得
  private string GetScreenShotPath()
  {
    DateTime now = DateTime.Now;
    string filename = "gcd_stage_" + now.ToString("yy_MM_dd_HH_mm_ss");

    string path = StandaloneFileBrowser.SaveFilePanel( "保村先を選んでください。", 
      System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), filename, "png" );
    return path;
  }

  private IEnumerator SavePNGAndJSON() {
    // スクリーン上のレンダリングが完了するまで待つ。
    yield return new WaitForEndOfFrame();
    // 保存画像サイズ
    int width = Screen.width;      
    int height = Screen.height;
    Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
    tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
    tex.Apply();

    byte [] pngData = tex.EncodeToPNG();
    string screenShotPath = GetScreenShotPath();

    File.WriteAllBytes(screenShotPath, pngData);    

    string json = toJSON();
    SaveJSONToPath(json, screenShotPath.Replace(".png", ".json"));
  }

  public static void SaveJSONToPath(string json, string filePath) {
    using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write)) {
      using (StreamWriter sw = new StreamWriter(fs)) {
        sw.Write(json);
      }
    }
  }
}

