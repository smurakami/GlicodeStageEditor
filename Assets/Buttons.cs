﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour {

	MapChip sampleChip;
	MainObject main;

	// Use this for initialization
	void Start () {
		sampleChip = GameObject.Find("SampleChip").GetComponent<MapChip>();
		main = GameObject.Find("MainObject").GetComponent<MainObject>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnEraseButton() {
		sampleChip.type = MapChip.Type.None;
		sampleChip.top = MapChip.Top.None;
		sampleChip.UpdateState();
	}

	public void OnFieldButton() {
		sampleChip.type = MapChip.Type.Normal;
		sampleChip.top = MapChip.Top.None;
		sampleChip.UpdateState();
	}

	public void OnJumpButton() {
		sampleChip.type = MapChip.Type.Jump;
		sampleChip.top = MapChip.Top.None;
		sampleChip.UpdateState();
	}

	public void OnTreeButton() {
		sampleChip.type = MapChip.Type.Normal;
		sampleChip.top = MapChip.Top.Tree;
		sampleChip.UpdateState();
	}

	public void OnWaterButton() {
		sampleChip.type = MapChip.Type.River;
		sampleChip.top = MapChip.Top.None;
		sampleChip.UpdateState();
	}

	public void OnStartButton() {
		sampleChip.type = MapChip.Type.Normal;
		sampleChip.top = MapChip.Top.Enter;
		sampleChip.UpdateState();
	}

	public void OnGoalButton() {
		sampleChip.type = MapChip.Type.Normal;
		sampleChip.top = MapChip.Top.Goal;
		sampleChip.UpdateState();
	}

	public void OnPipeButton() {
		sampleChip.type = MapChip.Type.Normal;
		sampleChip.top = MapChip.Top.Pipe;
		sampleChip.UpdateState();
	}

	public void OnStoneButton() {
		sampleChip.type = MapChip.Type.Normal;
		sampleChip.top = MapChip.Top.Stone;
		sampleChip.UpdateState();
	}

	public void OnUpButton() {
		sampleChip.alt += 1;
		sampleChip.UpdateState();
	}

	public void OnDownButton() {
		sampleChip.alt -= 1;
		if (sampleChip.alt < 0) {
			sampleChip.alt = 0;
		}
		sampleChip.UpdateState();
	}

	public void OnRotateButton() {
		sampleChip.direction += 1;
		if (sampleChip.direction >= 4) {
			sampleChip.direction = 0;
		}
		sampleChip.UpdateState();
	}

	public void OnSaveButton() {
		main.Save();
	}

	public void OnLoadButton() {
		main.Load();
	}
}
