﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapChipJSON {
	public string t;
	public string a;
}

public class MapChip : MonoBehaviour {

	public class Index {
		public int i;
		public int j;
	}

	public enum Type { None, Normal, Jump, River }
	public enum Top { None, Tree, Stone, Pipe, Enter, Goal }

	public Type type = Type.None;
	public int alt = 0;
	public int direction = 0;
	public Top top = Top.None;
	public Index index = new Index();

	public GameObject fieldPrefab;
	public GameObject riverPrefab;
	public GameObject treePrefab;
	public GameObject jumpPrefab;
	public GameObject pipePrefab;
	public GameObject stonePrefab;
	public GameObject enterPrefab;
	public GameObject goalPrefab;
	MapChip sampleChip;
	MainObject main;

	List<GameObject> fields = new List<GameObject>();
	GameObject topObject;
	GameObject jumpObject;
	GameObject riverObject;

	// Use this for initialization
	void Start () {
		sampleChip = GameObject.Find("SampleChip").GetComponent<MapChip>();
		main = GameObject.Find("MainObject").GetComponent<MainObject>();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnMouseDown() {
		if (main.isRotatingCamera) {
			return;
		}
  	ReadParamFromSampleChip();
		UpdateState();
	}

	void OnMouseEnter() {
		if (main.isRotatingCamera) {
			return;
		}

		if (Input.GetMouseButton(0)) {
			ReadParamFromSampleChip();
			UpdateState();
		}
	}

	public void ReadParamFromSampleChip() {
		alt = sampleChip.alt;
		type = sampleChip.type;
		top = sampleChip.top;
		direction = sampleChip.direction;
	}

	void EraseAllFields() {
    while (fields.Count > 0) {
    	GameObject obj = fields[fields.Count - 1];
    	fields.RemoveAt(fields.Count - 1);
    	Destroy(obj);
    }

    if (riverObject) {
    	Destroy(riverObject);
    	riverObject = null;
    }

    if (topObject) {
    	Destroy(topObject);
    	topObject = null;
    }

    if (jumpObject) {
    	Destroy(jumpObject);
    	jumpObject = null;
    }
	}

	void PutTop(GameObject prefab) {
	    GameObject obj = Instantiate(prefab, transform.position, Quaternion.identity);
	    float unitHeight = fieldPrefab.transform.localScale.y;
    	float currentHeight = fields.Count * unitHeight;
    	if (top != Top.Goal) {
	    	obj.transform.Rotate(0, 90f * direction, 0);
    	}
	    obj.transform.Translate(0, currentHeight, 0);
	    obj.transform.parent = transform;
	    topObject = obj;
	}

	public void UpdateState() {
		EraseAllFields();

		if (type == Type.None) {
			return;
		}

		if (type == Type.River) {
	    GameObject obj = Instantiate(riverPrefab, transform.position, Quaternion.identity);
	    float unitHeight = obj.transform.localScale.y;
	    obj.transform.Translate(0, unitHeight/2, 0);
	    obj.transform.parent = transform;
	    riverObject = obj;
	    return;
		}

		if (type == Type.Jump) {
	    GameObject obj = Instantiate(jumpPrefab, transform.position, Quaternion.identity);
	    float unitHeight = fieldPrefab.transform.localScale.y;
    	float currentHeight = alt * unitHeight + obj.transform.localScale.y/4;
	    obj.transform.Translate(0, currentHeight, 0);
	    obj.transform.parent = transform;
	    jumpObject = obj;
	    return;
		}

    while (fields.Count <= alt) {
	    GameObject obj = Instantiate(fieldPrefab, transform.position, Quaternion.identity);
	    float unitHeight = obj.transform.localScale.y;
    	float currentHeight = fields.Count * unitHeight + unitHeight/2;
	    obj.transform.Translate(0, currentHeight, 0);
	    obj.transform.parent = transform;
	    fields.Add(obj);
    } 

    if (top == Top.Tree) {
    	PutTop(treePrefab);
    }

    if (top == Top.Enter) {
    	PutTop(enterPrefab);
    }

    if (top == Top.Goal) {
    	PutTop(goalPrefab);
    }

    if (top == Top.Stone) {
    	PutTop(stonePrefab);
    }

    if (top == Top.Pipe) {
    	PutTop(pipePrefab);
    }
	}

	public string toJSON() {
		MapChipJSON json = new MapChipJSON();

		switch (type) {
			case Type.None:
			json.t = "empty_";
			break;

			case Type.River:
			json.t = "river_";
			break;
			case Type.Jump:
			json.t = "jump__";
			break;
			case Type.Normal:

			int num = 0;
			switch (top) {
				case Top.None:
				json.t = "normal";
				break;

				case Top.Tree:
				json.t = "tree__";
				break;

				case Top.Stone:
				json.t = "stone_";
				break;

				case Top.Enter:
				num = direction + 2 % 4;
				json.t = "entry" + num;
				break;

				case Top.Goal:
				json.t = "goal__";
				break;

				case Top.Pipe:
				num = direction % 2;
				json.t = "pipe" + num + "_";
				break;
			}
			break;
		}

		json.a = alt.ToString();
		return JsonUtility.ToJson(json);
	}

	public void readJSON(string json){
		MapChipJSON data = JsonUtility.FromJson<MapChipJSON>(json);
		alt = int.Parse(data.a);
		if (data.t.Contains("empty_")) {
			type = Type.None;
			top = Top.None;
		}
		if (data.t.Contains("river_")) {
			type = Type.River;
			top = Top.None;
		}
		if (data.t.Contains("jump__")) {
			type = Type.Jump;
			top = Top.None;
		}
		if (data.t.Contains("normal")) {
			type = Type.Normal;
			top = Top.None;
		}
		if (data.t.Contains("tree__")) {
			type = Type.Normal;
			top = Top.Tree;
		}
		if (data.t.Contains("stone_")) {
			type = Type.Normal;
			top = Top.Stone;
		}
		if (data.t.Contains("entry")) {
			type = Type.Normal;
			top = Top.Enter;
			int direction = int.Parse(data.t.Substring(5, 1));
			this.direction = direction + 2 % 4;
		}
		if (data.t.Contains("goal__")) {
			type = Type.Normal;
			top = Top.Goal;
		}
		if (data.t.Contains("pipe")) {
			type = Type.Normal;
			top = Top.Pipe;
			int direction = int.Parse(data.t.Substring(4, 1));
			this.direction = direction;
		}

		UpdateState();
	}
}
